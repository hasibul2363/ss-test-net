# Product Catalog
A sample application to demonstrate CRUD operation using ASP.net core 2.1 (back-end) and Vue Js (front-end)
## Back-end
### Requirements
- .NET Core 2.1
- MS SQL Server 2012 or higher
- Visual Studio 2017 v15.8 
### Running the back-end project
1. Open `src\back-end\ProductCatalog.All.sln`
2. Set database connection string to `appsettings.Development.json` or `appsettings.json` based on `launchSettings.json`. Here is full json path `src\back-end\businessApp\ProductCatalog.WebService\appsettings.Development.json`
3. Set logFilePath `(optional)`
4.	Build and Run from visual studio 
## Front-end
### Requirements
- Node JS 10+
- Vue CLI 3
### Running front-end project
1. Set API Base url to `src\front-end\src\config.js` file
2. Go to `src\front-end` directory
3. `npm install`
3. `npm run serve` (for dev)
## Note
- API documentation available on `http://[host-address]/swagger` (ex http://localhost:5000/swagger)
- API versioning: For versioning `Microsoft.AspNetCore.Mvc.Versioning` has been used and QueryController has different version with different model. Making version on deployment could be another approach.
- To prepare the demo a basic version of **CQRS** has been implemented. Here an in-memory bus has been used as a demo purpose but in real it should be replaced with Masstransit/ NserviceBus/ AzureServiceBus etc.
- Test: ProductCatalog.IntegrationTest is an integration test| Bus.Test, Repository.Test etc are unit tests
- Reusable components: All modules inside `src\back-end\infrastructure` folder are reusable component. They can be used for building other demo project.
- Logging: Structured Logging has been used (Serilog). By default console log has enabled and for file based logging just need to add file path on `appsettings.Development.json` or `appsettings.json` file.

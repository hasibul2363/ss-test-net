﻿using System;

namespace SuitSupply.ProductCatalog.WebService.Controllers.v2
{
    public class QueryModel
    {
        public QueryModel()
        {
            PageNumber = 1;
            PageSize = 10;
        }
        public string ItemName { get; set; }
        public string ItemCode { get; set; }
        public Guid Id { get; set; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }

    }
}

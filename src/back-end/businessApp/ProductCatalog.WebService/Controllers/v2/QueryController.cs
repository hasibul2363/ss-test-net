﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SuitSupply.Infrastructure.Bootstrapper;
using SuitSupply.Infrastructure.Bus.Contracts;
using SuitSupply.Infrastructure.Bus.Contracts.Query;
using SuitSupply.Infrastructure.Logger.Contracts;
using SuitSupply.ProductCatalog.DomainModels;
using SuitSupply.ProductCatalog.Queries;

namespace SuitSupply.ProductCatalog.WebService.Controllers.v2
{
    [ApiVersion("2.0")]
    [Route(SuitWebApiBootstrapper.VersionedRoutePattern)]
    public class QueryController : Controller
    {
        public ISuitBus Bus { get; set; }
        public ISuitLog Log { get; set; }


        public QueryController(ISuitBus bus, ISuitLog log)
        {
            Bus = bus;
            Log = log;
        }

        [HttpPost]
        public Task<QueryResponse<List<Product>>> ProductQuery([FromBody] QueryModel query)
        {
            var productQuery = new ProductQuery
            {
                Id = query.Id,
                Code = query.ItemCode,
                Name = query.ItemName,
                PageSize = query.PageSize,
                PageNumber = query.PageNumber
            };
            Log.Information("Just received query v2 for processing with details {@queryModel}", query);
            return Bus.Query(productQuery);
        }

        [HttpGet]
        public async Task<IActionResult> DoExcelExport([FromQuery] string name, [FromQuery]string code)
        {
            Log.Information("Just received export request for processing with details {@code}, {@name}", code, name);
            var query = new ExcelExportQuery
            {
                Name = name,
                Code = code
            };
            var response = await Bus.Query(query);
            Log.Information("export request has processed successfully with details {@code}, {@name}", code, name);
            return File(response.Data, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "ProductCatalog.xlsx");
        }
    }
}
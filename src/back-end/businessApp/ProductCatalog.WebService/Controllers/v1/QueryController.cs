﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SuitSupply.Infrastructure.Bootstrapper;
using SuitSupply.Infrastructure.Bus.Contracts;
using SuitSupply.Infrastructure.Bus.Contracts.Query;
using SuitSupply.Infrastructure.Logger.Contracts;
using SuitSupply.ProductCatalog.DomainModels;
using SuitSupply.ProductCatalog.Queries;

namespace SuitSupply.ProductCatalog.WebService.Controllers.v1
{
    [ApiVersion("1.0")]
    [Route(SuitWebApiBootstrapper.VersionedRoutePattern)]
    public class QueryController : Controller
    {
        public ISuitBus Bus { get; set; }
        public ISuitLog Log { get; set; }


        public QueryController(ISuitBus bus, ISuitLog log)
        {
            Bus = bus;
            Log = log;
        }

        [HttpPost]
        public Task<QueryResponse<List<Product>>> ProductQuery([FromBody] ProductQuery query)
        {
            Log.Information("Just received query for processing with details {@query}", query);
            return Bus.Query(query);
        }

        [HttpGet]
        public async Task<IActionResult> DoExcelExport([FromQuery] string name, [FromQuery]string code)
        {
            Log.Information("Just received export request for processing with details {@code}, {@name}", code, name);
            var query = new ExcelExportQuery
            {
                Name = name,
                Code = code
            };
            var response = await Bus.Query(query);
            Log.Information("export request has processed successfully with details {@code}, {@name}", code, name);
            return File(response.Data, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "ProductCatalog.xlsx");
        }
    }
}
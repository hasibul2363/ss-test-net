﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SuitSupply.Infrastructure.Bootstrapper;
using SuitSupply.Infrastructure.Bus.Contracts;
using SuitSupply.ProductCatalog.Commands;

namespace SuitSupply.ProductCatalog.WebService.Controllers
{
    [Route(SuitWebApiBootstrapper.VersionedRoutePattern)]
    public class ValuesController : Controller
    {
        [HttpGet]
        public async Task<string> Get()
        {
            return "running..." ;
        }
    }
}

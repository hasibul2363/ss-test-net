﻿using AutoMapper;
using DataContext;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SuitSupply.Infrastructure.Bootstrapper;
using SuitSupply.Infrastructure.Bus;
using SuitSupply.Infrastructure.Bus.Command;
using SuitSupply.Infrastructure.Bus.Contracts;
using SuitSupply.Infrastructure.Bus.Query;
using SuitSupply.Infrastructure.Logger.Contracts;
using SuitSupply.Infrastructure.Logger.Serilog;
using SuitSupply.Infrastructure.Repository;
using SuitSupply.Infrastructure.Repository.Contracts;
using SuitSupply.Infrastructure.SLAdapter.MsDependency;
using SuitSupply.Infrastructure.Validator.Contract;
using SuitSupply.ProductCatalog.CommandHandlers;
using SuitSupply.ProductCatalog.Commands;
using SuitSupply.ProductCatalog.DomainModels;
using SuitSupply.ProductCatalog.Queries;
using SuitSupply.ProductCatalog.QueryHandlers;
using SuitSupply.ProductCatalog.Validators;
using Swashbuckle.AspNetCore.Swagger;
using System.Collections.Generic;
using System.IO;
using Microsoft.AspNetCore.Mvc.ApiExplorer;

namespace SuitSupply.ProductCatalog.WebService
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        
        public void ConfigureServices(IServiceCollection services)
        {
            SeriLogConfiguration.Configure(Configuration.GetSection("logFilePath").Value);

            RegisterDependencies(services);

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            Mapper.Initialize(cfg => {
                cfg.CreateMap<CreateProductCommand, Product>();
                cfg.CreateMap<UpdateProductCommand, Product>();
            });

            AddVersioningAndDoc(services);
        }

        
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, IApiVersionDescriptionProvider provider)
        {
            if (env.IsDevelopment())
                app.UseDeveloperExceptionPage();

            SuitWebApiBootstrapper.Use(app);

            var context = CommonServiceLocator.ServiceLocator.Current.GetInstance<BaseContext>();
            context.Database.EnsureCreatedAsync();

            app.UseSwagger();
            app.UseSwaggerUI(
                options =>
                {
                    foreach (var description in provider.ApiVersionDescriptions)
                    {
                        options.SwaggerEndpoint(
                            $"/swagger/{description.GroupName}/swagger.json",
                            description.GroupName.ToUpperInvariant());
                    }
                });
        }

        private void RegisterDependencies(IServiceCollection services)
        {
            services.AddScoped<ISuitValidator<ProductQuery>, ProductQueryValidator>();
            services.AddScoped<SuitQueryHandler<List<Product>, ProductQuery>, ProductQueryHandler>();
            services.AddScoped<SuitQueryHandler<MemoryStream, ExcelExportQuery>, ExcelExportQueryHandler>();

            services.AddSingleton<ISuitLog, SuitLogUsingSerilog>();
            services.AddScoped<ISuitBus, SuitInmemoryBus>();
            services.AddScoped<ISuitValidator<CreateProductCommand>, CreateProductCommandValidator>();
            services.AddScoped<SuitCommandHandler<CreateProductCommand>, CreateProductCommandHandler>();

            services.AddScoped<ISuitValidator<DeleteProductCommand>, DeleteProductCommandValidator>();
            services.AddScoped<SuitCommandHandler<DeleteProductCommand>, DeleteProductCommandHandler>();

            services.AddScoped<ISuitValidator<UpdateProductCommand>, UpdateProductCommandValidator>();
            services.AddScoped<SuitCommandHandler<UpdateProductCommand>, UpdateProductCommandHandler>();

            services.AddScoped<IRepository, Repository>();
            services.AddScoped<IReadOnlyRepository, Repository>();

            var contextBuilder = new DbContextOptionsBuilder();
            contextBuilder.UseSqlServer(Configuration.GetSection("dbConnectionString").Value);
            services.AddSingleton(contextBuilder.Options);
            services.AddScoped<BaseContext, ProductCatalogDbContext>();

            var adapter = new MsServiceLocatorAdapter(services);
            CommonServiceLocator.ServiceLocator.SetLocatorProvider(() => adapter);
        }
        private void AddVersioningAndDoc(IServiceCollection services)
        {
            services.AddVersionedApiExplorer(
                options =>
                {
                    options.GroupNameFormat = "'v'VVV";
                    options.SubstituteApiVersionInUrl = true;
                });

            services.AddApiVersioning(o =>
            {
                o.AssumeDefaultVersionWhenUnspecified = true;
                o.DefaultApiVersion = new ApiVersion(1, 0);
                o.ReportApiVersions = true;
            });

            services.AddSwaggerGen(
                options =>
                {
                    var provider = services.BuildServiceProvider()
                        .GetRequiredService<IApiVersionDescriptionProvider>();

                    foreach (var description in provider.ApiVersionDescriptions)
                    {
                        options.SwaggerDoc(
                            description.GroupName,
                            new Info()
                            {
                                Title = $"Sample API {description.ApiVersion}",
                                Version = description.ApiVersion.ToString()
                            });
                    }
                });
        }
    }
}

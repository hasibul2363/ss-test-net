﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SuitSupply.Infrastructure.Bus.Contracts;
using SuitSupply.Infrastructure.Repository.Contracts;
using SuitSupply.ProductCatalog.Commands;
using SuitSupply.ProductCatalog.DomainModels;

namespace ProductCatalog.IntegrationTest.Commands
{
    [TestClass]
    public class UpdateProductCommandTest : TestBase
    {
        public ISuitBus SuitBus { get; set; }
        public IReadOnlyRepository Repository { get; set; }

        [TestInitialize]
        public void InitTest()
        {
            Init();
            SuitBus = ServiceProvider.GetService<ISuitBus>();
            Repository = ServiceProvider.GetService<IReadOnlyRepository>();
        }

        [TestMethod]
        public async Task UpdateProductCommandMustSucceed()
        {
            var createProductCommand = TestDataProvider.GetCreateProductCommand();
            var responseCreate = await SuitBus.Send(createProductCommand);
            if (responseCreate.Success)
            {
                var oldProduct = await Repository.GetItem<Product>(p => p.Id == createProductCommand.Id);
                var updateProductCommand = TestDataProvider.GetUpdateProductCommand(createProductCommand.Id);
                var response = await SuitBus.Send(updateProductCommand);
                var newProduct = Repository.GetItems<Product>(p => p.Id == updateProductCommand.Id).First();
                Assert.AreEqual(true, response.Success);
                Assert.AreNotEqual(oldProduct.Code, newProduct.Code);
            }

        }
        [TestMethod]
        public async Task UpdateProductCommandMustFailed()
        {
            var updateProductCommand = TestDataProvider.GetUpdateProductCommand();
            updateProductCommand.Name = string.Empty;
            var response = await SuitBus.Send(updateProductCommand);
            Assert.AreNotEqual(0, response.ValidationResult.Errors.Count);
            Assert.AreEqual(false, response.Success);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using DocumentFormat.OpenXml.InkML;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SuitSupply.Infrastructure.Bus.Contracts;
using SuitSupply.Infrastructure.Repository.Contracts;
using SuitSupply.ProductCatalog.Commands;
using SuitSupply.ProductCatalog.DomainModels;

namespace ProductCatalog.IntegrationTest.Commands
{
    [TestClass]
    public class CreateProductCommandTest : TestBase
    {
        public ISuitBus SuitBus { get; set; }
        public IReadOnlyRepository Repository { get; set; }

        [TestInitialize]
        public void InitTest()
        {
            Init();
            SuitBus = ServiceProvider.GetService<ISuitBus>();
            Repository = ServiceProvider.GetService<IReadOnlyRepository>();
        }

        [TestMethod]
        public async Task CreateProductCommandMustSucceed()
        {
        
            var createProductCommand = TestDataProvider.GetCreateProductCommand();
            var response = await SuitBus.Send(createProductCommand);
            Assert.AreEqual(true,response.Success);
            var product = await Repository.GetItem<Product>(p => p.Id == createProductCommand.Id);
            Assert.AreNotEqual(null,product);
            Assert.AreEqual(createProductCommand.Name,product.Name);

        }

        [TestMethod]
        public async Task CreateProductCommandMustFailed()
        {
            var createProductCommand = TestDataProvider.GetCreateProductCommand();
            createProductCommand.Name = string.Empty;
            createProductCommand.Id = Guid.Empty;
            var response = await SuitBus.Send(createProductCommand);
            Assert.AreNotEqual(0, response.ValidationResult.Errors.Count);
            Assert.AreEqual(false, response.Success);
        }
    }
}

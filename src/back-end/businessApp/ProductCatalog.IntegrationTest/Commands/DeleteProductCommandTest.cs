﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SuitSupply.Infrastructure.Bus.Contracts;
using SuitSupply.Infrastructure.Repository.Contracts;
using SuitSupply.ProductCatalog.Commands;
using SuitSupply.ProductCatalog.DomainModels;

namespace ProductCatalog.IntegrationTest.Commands
{
    [TestClass]
    public class DeleteProductCommandTest:TestBase
    {
        public ISuitBus SuitBus { get; set; }
        public IReadOnlyRepository Repository { get; set; }
        [TestInitialize]
        public void InitTest()
        {
            Init();
            SuitBus = ServiceProvider.GetService<ISuitBus>();
            Repository = ServiceProvider.GetService<IReadOnlyRepository>();
        }

        [TestMethod]
        public async Task DeleteProductCommandMustSucceed()
        {
            
            var createProductCommand = TestDataProvider.GetCreateProductCommand(Guid.NewGuid());
            await SuitBus.Send(createProductCommand);
            var existingItems = Repository.GetItems<Product>(p => p.Id == createProductCommand.Id);
            Assert.AreEqual(true, existingItems.Any());
            var response = await SuitBus.Send(new DeleteProductCommand
            {
                Id = createProductCommand .Id
            });

            Assert.AreEqual(true,response.Success);
            var itemsAfterDelete = Repository.GetItems<Product>(p => p.Id == createProductCommand.Id);
            Assert.AreEqual(false, itemsAfterDelete.Any());
        }

      
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using SuitSupply.ProductCatalog.Commands;

namespace ProductCatalog.IntegrationTest
{
    public class TestDataProvider
    {
        public static CreateProductCommand GetCreateProductCommand(Guid? id = null)
        {
            id = id ?? Guid.NewGuid();
            return new CreateProductCommand
            {
                Id = (Guid)id,
                Code = Guid.NewGuid().ToString(),
                Name = "TestName01",
                PhotoUrl = "some url",
                Price = 250
            };
        }
        public static UpdateProductCommand GetUpdateProductCommand(Guid? id = null)
        {
            id = id ?? Guid.NewGuid();
            return new UpdateProductCommand
            {
                Id = (Guid)id,
                Code = "TestCode22",
                Name = "TestName22",
                PhotoUrl = "some url",
                Price = 250
            };
        }
    }
}

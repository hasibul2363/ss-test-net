﻿using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SuitSupply.Infrastructure.Bus.Contracts;
using SuitSupply.ProductCatalog.Queries;

namespace ProductCatalog.IntegrationTest.Queries
{
    [TestClass]
    public class ProductQueryTest : TestBase
    {
        public ISuitBus SuitBus { get; set; }

        [TestInitialize]
        public void InitTest()
        {
            Init();
            SuitBus = ServiceProvider.GetService<ISuitBus>();
        }

        [TestMethod]
        public async Task ProductQueryMustSucceed()
        {

            var createProductCommand = TestDataProvider.GetCreateProductCommand();
            var response = await SuitBus.Send(createProductCommand);
            var query = new ProductQuery
            {
                PageNumber = 1, PageSize = 10
            };
            var queryResponse = await SuitBus.Query(query);
            Assert.AreEqual(true, queryResponse.Success);
            Assert.AreEqual(true, queryResponse.Data.Any());
        }

        
    }
}

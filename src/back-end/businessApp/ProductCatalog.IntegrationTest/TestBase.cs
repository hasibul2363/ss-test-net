﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using AutoMapper;
using DataContext;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using SuitSupply.Infrastructure.Bus;
using SuitSupply.Infrastructure.Bus.Command;
using SuitSupply.Infrastructure.Bus.Contracts;
using SuitSupply.Infrastructure.Bus.Query;
using SuitSupply.Infrastructure.Logger.Contracts;
using SuitSupply.Infrastructure.Logger.Serilog;
using SuitSupply.Infrastructure.Repository;
using SuitSupply.Infrastructure.Repository.Contracts;
using SuitSupply.Infrastructure.SLAdapter.MsDependency;
using SuitSupply.Infrastructure.Validator.Contract;
using SuitSupply.ProductCatalog.CommandHandlers;
using SuitSupply.ProductCatalog.Commands;
using SuitSupply.ProductCatalog.DomainModels;
using SuitSupply.ProductCatalog.Queries;
using SuitSupply.ProductCatalog.QueryHandlers;
using SuitSupply.ProductCatalog.Validators;

namespace ProductCatalog.IntegrationTest
{
    public class TestBase
    {
        private static bool _mapperHasInitialized = false;
        public IServiceProvider ServiceProvider { get; set; }

        public void Init()
        {
            var services = new ServiceCollection();
            SeriLogConfiguration.Configure();

            services.AddTransient<ISuitValidator<ProductQuery>, ProductQueryValidator>();
            services.AddTransient<SuitQueryHandler<List<Product>, ProductQuery>, ProductQueryHandler>();
            services.AddTransient<SuitQueryHandler<MemoryStream, ExcelExportQuery>, ExcelExportQueryHandler>();

            services.AddSingleton<ISuitLog, SuitLogUsingSerilog>();
            services.AddTransient<ISuitBus, SuitInmemoryBus>();
            services.AddTransient<ISuitValidator<CreateProductCommand>, CreateProductCommandValidator>();
            services.AddTransient<SuitCommandHandler<CreateProductCommand>, CreateProductCommandHandler>();

            services.AddTransient<ISuitValidator<DeleteProductCommand>, DeleteProductCommandValidator>();
            services.AddTransient<SuitCommandHandler<DeleteProductCommand>, DeleteProductCommandHandler>();

            services.AddTransient<ISuitValidator<UpdateProductCommand>, UpdateProductCommandValidator>();
            services.AddTransient<SuitCommandHandler<UpdateProductCommand>, UpdateProductCommandHandler>();

            services.AddTransient<IRepository, Repository>();
            services.AddTransient<IReadOnlyRepository, Repository>();

            var builder = new DbContextOptionsBuilder();
            builder.UseInMemoryDatabase(databaseName: "Add_writes_to_database");
            services.AddSingleton(builder.Options);
            services.AddSingleton<BaseContext, ProductCatalogDbContext>();

            var adapter = new MsServiceLocatorAdapter(services);
            CommonServiceLocator.ServiceLocator.SetLocatorProvider(() => adapter);

            if (!_mapperHasInitialized)
            {
                Mapper.Initialize(cfg =>
                {
                    cfg.CreateMap<CreateProductCommand, Product>();
                    cfg.CreateMap<UpdateProductCommand, Product>();
                });
                _mapperHasInitialized = true;
            }

            ServiceProvider = services.BuildServiceProvider();
        }
    }
}

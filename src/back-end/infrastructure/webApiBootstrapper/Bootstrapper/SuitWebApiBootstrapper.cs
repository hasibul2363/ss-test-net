﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Builder;
using SuitSupply.Infrastructure.Bootstrapper.Extensions;
using SuitSupply.Infrastructure.Bootstrapper.Middlewares;

namespace SuitSupply.Infrastructure.Bootstrapper
{
    public static class SuitWebApiBootstrapper
    {
        public static void Use(IApplicationBuilder builder, string route ="")
        {
            builder
                .UseMiddleware<ExceptionMiddlewareToLogError>()
                .UseCorsToAllowAll()
                .UseSuitRouting(route);

        }

        public const string VersionedRoutePattern = "api/v{api-version:apiVersion}/[controller]/[action]";
    }
}

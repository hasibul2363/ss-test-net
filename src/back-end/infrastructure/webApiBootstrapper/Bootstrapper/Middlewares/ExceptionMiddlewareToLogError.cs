﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using SuitSupply.Infrastructure.Logger.Contracts;

namespace SuitSupply.Infrastructure.Bootstrapper.Middlewares
{
    public class ExceptionMiddlewareToLogError
    {
        private readonly RequestDelegate _next;

        public ExceptionMiddlewareToLogError(RequestDelegate next)
        {
            _next = next;
        }

        public async Task InvokeAsync(HttpContext context,ISuitLog log)
        {
            try
            {
                await _next(context);
            }
            catch (Exception ex)
            {
                log.Fatal(ex,"Failed to process the request");
                throw;
            }
           
        }
    }
}
